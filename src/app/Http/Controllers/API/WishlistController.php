<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Goutte\Client;

class WishlistController extends Controller
{
    public function index() {
        return view('wishlist');
    }

    public function retrieveProductInfos(Request $request) {
        $client = new Client();

        $crawler = $client->request('GET', $request->url);
        $title = null;
        $price = null;

        $title = $crawler->filter('#productTitle')->first()->text();
        $image = $crawler->filter('#imgTagWrapperId img')->eq(0)->attr('src');
        $price = $crawler->filter('#priceblock_ourprice')->first()->text();

        return [
            "title" => $title,
            "price" => $price,
            "image" => $image
        ];

    }
}
