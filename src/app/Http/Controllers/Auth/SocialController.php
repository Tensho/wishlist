<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Socialite;

class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $userInfos = Socialite::driver($provider)->user();

        $user = $this->createUser($userInfos, $provider);

        auth()->login($user);

        return redirect()->to('/home');

    }

    protected function createUser($userInfos, $provider){

        $user = User::where('provider_id', $userInfos->id)->first();

        if (!$user) {
            $user = User::create([
                'name'        => $userInfos->name,
                'email'       => $userInfos->email,
                'image'       => $userInfos->avatar,
                'provider'    => $provider,
                'provider_id' => $userInfos->id
            ]);
        }

        return $user;
    }
}
