import Vue from "vue";
import App from "./app/App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount("#app");
