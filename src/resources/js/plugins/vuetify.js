import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import fr from "vuetify/es5/locale/fr";
import en from "vuetify/es5/locale/en";

Vue.use(Vuetify);

const opts = {
    lang: { locales: { fr, en }, current: "fr" },
    theme: {
        themes: {
            light: {
                primary: "#64b5f6",
                secondary: "#e890a6"
            }
        }
    }
};

export default new Vuetify(opts);
