export const routes = [
    {
        path: "/login",
        name: "login",
        component: require("../views/Login.vue").default
    },
    {
        path: "/register",
        name: "register",
        component: require("../views/Register.vue").default
    }
];
