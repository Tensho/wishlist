import Vue from "vue";
import Router from "vue-router";
import { routes as auth } from "@/app/auth/routes";
import { routes as pages } from "@/app/pages/routes";
import { routes as wishlist } from "@/app/wishlist/routes";

Vue.use(Router);

const routes = [...auth, ...pages, ...wishlist];

const router = new Router({
    mode: "history",
    routes,
    linkActiveClass: "active"
});

export default router;
